class Strings {
  static const String app_name = "King of The Spot";
  static const String login_with_facebook = "Login with Facebook";
  static const String sign_up = "Sign Up";
  static const String by_creating_an_account =
      "By creating an account you agree to";
  static const String terms_and_conditions = "Terms and Conditions";
  static const String already_have_an_account = "I already have an account";
}
