import 'package:flutter/material.dart';
import 'package:king_of_the_spot/utils/ColorUtils.dart';
import 'package:king_of_the_spot/utils/Strings.dart';

void main() => runApp(KingOfTheSpot());

class KingOfTheSpot extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'King Of The Spot',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: AuthenticationPage(title: Strings.app_name),
    );
  }
}

class AuthenticationPage extends StatefulWidget {
  AuthenticationPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _AuthenticationPageState createState() => _AuthenticationPageState();
}

class _AuthenticationPageState extends State<AuthenticationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
            child: Image.asset('assets/ic_logo.png',
                fit: BoxFit.fitWidth, alignment: Alignment.topCenter)),
        FractionalTranslation(
          translation: Offset(0.0, 0.2),
          child: Image.asset(
            'assets/ic_blurred_gray_background.png',
            fit: BoxFit.fitHeight,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 203),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/ic_black_crown.png',
              ),
              Container(
                margin: const EdgeInsets.only(top: 9),
                child: Image.asset(
                  'assets/ic_king_of_the_spot_black.png',
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 51),
                width: 234,
                height: 45,
                child: RaisedButton(
                  child: const Text(
                    Strings.login_with_facebook,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'RadikalMedium',
                        fontSize: 14),
                  ),
                  color: ColorUtils.facebook_color,
                  elevation: 9.0,
                  onPressed: () {},
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 19),
                width: 234,
                height: 45,
                child: RaisedButton(
                  child: const Text(
                    Strings.sign_up,
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'RadikalMedium',
                        fontSize: 14),
                  ),
                  color: ColorUtils.light_black,
                  elevation: 4.0,
                  onPressed: () {},
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 36),
                child: new RichText(
                  text: new TextSpan(
                    text: Strings.already_have_an_account,
                    style: new TextStyle(
                        decoration: TextDecoration.underline,
                        color: ColorUtils.light_black),
                  ),
                ),
              )
            ],
          ),
        ),
        Opacity(
          opacity: 0.5,
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                margin: const EdgeInsets.only(bottom: 14),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    new RichText(
                      textAlign: TextAlign.center,
                      text: new TextSpan(
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(
                              text: Strings.by_creating_an_account + '\n',
                              style: TextStyle(fontFamily: 'RadikalMedium')),
                          new TextSpan(
                              text: Strings.terms_and_conditions,
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                        ],
                      ),
                    )
                  ],
                )),
          ),
        ),
      ],
    )));
  }
}
